﻿using FreeSql;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// FreeSql 服务拓展类
    /// </summary>
    public static class FreeSqlServiceCollectionExtensions
    {
        /// <summary>
        /// 添加 FreeSql 拓展
        /// </summary>
        /// <param name="services"></param>
        /// <param name="connectionString"></param>
        /// <param name="dataType"></param>
        /// <param name="buildAction"></param>
        /// <param name="freeSqlAction"></param>
        /// <returns></returns>
        public static IServiceCollection AddFreeSql(this IServiceCollection services, string connectionString, DataType dataType, Action<FreeSqlBuilder> buildAction = default, Action<IFreeSql> freeSqlAction = default)
        {
            return services.AddFreeSql(freeSqlBuilder =>
            {
                freeSqlBuilder.UseConnectionString(dataType, connectionString);
                buildAction?.Invoke(freeSqlBuilder);
            }, freeSqlAction);
        }
        /// <summary>
        /// 添加 FreeSql 拓展
        /// </summary>
        /// <param name="services"></param>
        /// <param name="buildAction"></param>
        /// <param name="freeSqlAction"></param>
        /// <returns></returns>
        public static IServiceCollection AddFreeSql(this IServiceCollection services, Action<FreeSqlBuilder> buildAction = default, Action<IFreeSql> freeSqlAction = default)
        {
            services.AddScoped<UnitOfWorkManager>();

            services.AddSingleton(u =>
            {
                var freeSqlBuilder = new FreeSqlBuilder();

                buildAction?.Invoke(freeSqlBuilder);

                var freeSql = freeSqlBuilder.Build();

                freeSqlAction?.Invoke(freeSql);

                return freeSql;
            });

            // 注册非泛型仓储
            services.AddScoped<IFreeSqlRepository, FreeSqlRepository>();

            // 注册 FreeSql 仓储
            services.AddScoped(typeof(IFreeSqlRepository<>), typeof(FreeSqlRepository<>));
            services.AddScoped(typeof(IFreeSqlRepository<,>), typeof(FreeSqlRepository<,>));

            return services;
        }
    }
}
